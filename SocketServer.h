#pragma once
#include "Socket.h"

namespace vk::tcp::safe {
	class SocketServer : public vk::tcp::safe::Socket{
	private:
	protected:
		bool _bind()									noexcept;
		bool _listen()									noexcept;
	public:
		bool	open(port p)							noexcept;
		SocketServer()									= default;
		virtual ~SocketServer()							= default;
		SocketServer(const SocketServer&)			;
		SocketServer& operator=(const SocketServer&);
		SocketServer(SocketServer&&)				;
		SocketServer& operator=(SocketServer&&)		;

	};
}


namespace vk::tcp::unsafe {
class SocketServer : public vk::tcp::unsafe::Socket {
private:
protected:
	bool _bind()									noexcept;
	bool _listen()									noexcept;
public:
	bool	open(port p)							noexcept;
	SocketServer()									= default;
	virtual ~SocketServer()							= default;
	SocketServer(const SocketServer&);
	SocketServer& operator=(const SocketServer&);
	SocketServer(SocketServer&&);
	SocketServer& operator=(SocketServer&&);

};
}