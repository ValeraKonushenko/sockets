#include "_WSAData.h"
#pragma comment(lib, "Ws2_32.lib")

WSADATA* _WSAData::startup() {
	std::lock_guard<std::mutex> lg(mut);
	if (is_init)return &wsaData;
	if (WSAStartup(0x0101, &wsaData)) {
		throw std::exception("WSAStartup was failed");
	}
	is_init = true;
	return &wsaData;
}

_WSAData::Destroyer::Destroyer(const bool* const is_init)
	:is_init(is_init){}

_WSAData::Destroyer::~Destroyer(){
	if(*is_init)
		WSACleanup();
}

bool				_WSAData::is_init = false;
std::mutex			_WSAData::mut;
WSADATA				_WSAData::wsaData = {};
_WSAData::Destroyer _WSAData::destroyer(&_WSAData::is_init);

