#include "Acceptor.h"
#include <thread>

namespace vk::safe{
void				Acceptor::accept	(SocketConnection sc) {
	using namespace std;
	SOCKADDR_IN from{};
	int fromlen = sizeof(from);
	SOCKET sfrom = 
		::accept(sc.socket(), reinterpret_cast<struct sockaddr*>(&from), &fromlen);

	SocketConnection tmp;
	tmp.port(sc.port());
	tmp.sockaddr(from);
	tmp.socket(sfrom);
	std::lock_guard lg(recmut);
	vec.push_back(tmp);
	safe::Logger::push(Log("Client was connected!\n"));
}
size_t				Acceptor::size		()const{
	std::lock_guard lg(recmut);
	return this->vec.size();
}
SocketConnection	Acceptor::operator[](int pos)const noexcept {
	std::lock_guard lg(recmut);
	int sz = static_cast<int>(vec.size());
	while (pos >= sz && sz)		pos -= sz;
	while (pos < 0)				pos += sz;
	return vec[pos];
}
SocketConnection	Acceptor::end		() const noexcept{
	std::lock_guard lg(recmut);
	return vec[vec.size() - 1];
}
void Acceptor::dropConnection(int pos) noexcept{
	std::lock_guard lg(recmut);
	int sz = static_cast<int>(vec.size());
	while (pos >= sz && sz)		pos -= sz;
	while (pos < 0)				pos += sz;
	vec.erase(vec.begin() + pos);
}
Acceptor::Acceptor(const Acceptor& obj)				noexcept{
	std::scoped_lock sl(recmut, obj.recmut);	
	*this = obj;
}
Acceptor&			Acceptor::operator=	(const Acceptor& obj)	noexcept{
	std::scoped_lock sl(recmut, obj.recmut);	
	this->vec = obj.vec;
	return *this;
}
Acceptor::Acceptor(Acceptor&& obj)					noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	*this = std::move(obj);
}
Acceptor&			Acceptor::operator=	(Acceptor&& obj)		noexcept{
	std::scoped_lock sl(recmut, obj.recmut);	
	this->vec = std::move(obj.vec);
	return *this;
}
}






namespace vk::unsafe{
void				Acceptor::accept(SocketConnection sc) {
	using namespace std;

	SOCKADDR_IN from{};
	int fromlen = sizeof(from);
	SOCKET sfrom = 
		::accept(sc.socket(), reinterpret_cast<struct sockaddr*>(&from), &fromlen);

	SocketConnection tmp;
	tmp.port(sc.port());
	tmp.sockaddr(from);
	tmp.socket(sfrom);
	vec.push_back(tmp);
	safe::Logger::push(Log("Client was connected!\n"));
}
size_t				Acceptor::size()const{
	return this->vec.size();
}
SocketConnection	Acceptor::operator[](int pos)const noexcept {
	int sz = static_cast<int>(vec.size());
	while (pos >= sz && sz)		pos -= sz;
	while (pos < 0)				pos += sz;
	return vec[pos];
}
SocketConnection	Acceptor::end() const noexcept{
	return vec[vec.size() - 1];
}
Acceptor::Acceptor(const Acceptor& obj)				noexcept{
	*this = obj;
}
Acceptor&			Acceptor::operator=(const Acceptor& obj)	noexcept{
	this->vec = obj.vec;
	return *this;
}
Acceptor::Acceptor(Acceptor&& obj)					noexcept{
	*this = std::move(obj);
}
Acceptor&			Acceptor::operator=(Acceptor&& obj)		noexcept{
	this->vec = std::move(obj.vec);
	return *this;
}
}