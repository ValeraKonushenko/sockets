#include "Time.h"

std::string vk::Time::getLocalTime(time_t unixtime) {
	tm info{};
	int size = 128;
	char* p = new char[size];
	localtime_s(&info, &unixtime);
	asctime_s(p, size, &info);
	p[strlen(p) - 1] = 0;
	std::string str(p);
	delete[]p;
	return std::move(str);
}
std::string vk::Time::getLocalTime() {
	tm info{};
	time_t unixtime{};
	int size = 128;
	char* p = new char[size];
	localtime_s(&info, &unixtime);
	asctime_s(p, size, &info);
	std::string str(p);
	delete[]p;
	return std::move(str);
}
