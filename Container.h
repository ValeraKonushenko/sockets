#pragma once
namespace vk::unsafe {
/*
Container - is wrapper of some pointer to array.

-Fast start:
vk::unsafe::Container<char> c;
c.set("Hello", 5);


T*		data	() noexcept -
return a pointer to data
Example:
vk::unsafe::Container<char> c;
c.set("Hello", 5);
for(size_t i = 0u; i < c.getSize();i++)
	cout << c.data()[i];


bool	isEmpty	() const noexcept - 
return true if the array is empty and false if is not empty
Example:
vk::unsafe::Container<char> c;
c.set("Hello", 5);
if(c.isEmpty())cout << "no data\n";


size_t	getSize	() const noexcept - 
return the size of an inner array
Example:
vk::unsafe::Container<char> c;
c.set("Hello", 5);
cout << "Size: " << c.getSize();//5


void	clear	() noexcept -
clear all data with allocated memory
Example:
vk::unsafe::Container<char> c;
c.set("Hello", 5);
cout << "Size: " << c.getSize();//5
c.clear();
cout << "Size: " << c.getSize();//0


void	reserve	(size_t sz)noexcept - 
clear old data and allocate new memory by new size
Example:
vk::unsafe::Container<char> c;
c.reserve(5);
cout << "Size: " << c.getSize();//5


void	set		(T t)noexcept - 
fill all allocated memory like t parametr
Example:
vk::unsafe::Container<char> c;
c.reserve(10);
c.set('H');
for(size_t i = 0u; i < c.getSize();i++)
	cout << c.data()[i];
//HHHHHHHHHH


void	set		(const T* p, size_t sz)	noexcept - 
set data from source by special size
c.set("Hello", 5);
for(size_t i = 0u; i < c.getSize();i++)
	cout << c.data()[i];
//Hello



*/


template<class T>
class Container {
public:
protected:
	T* ptr;
	size_t size;
	void __zero_clear()noexcept;
public:
	T*					data			()						noexcept;
	bool				isEmpty			()						const noexcept;
	size_t				getSize			()						const noexcept;
	void				clear			()						noexcept;
	void				reserve			(size_t sz)throw(std::bad_alloc);
	void				set				(T t)					noexcept;
	void				set				(const T* p, size_t sz)	noexcept;
						Container		();
						Container		(size_t sz)throw(std::bad_alloc);
						~Container		();
						Container		(const Container&)		noexcept;
	Container&			operator=		(const Container&)		noexcept;
						Container		(Container&& obj)		noexcept;
	Container&			operator=		(Container&& obj)		noexcept;
						operator size_t	()const noexcept;
						operator const T*()const noexcept;
						operator T*		() noexcept;
};




typedef Container<char>		Container_c;
typedef Container<short>	Container_s;
typedef Container<int>		Container_i;






template<class T>
inline void Container<T>::__zero_clear() noexcept{
	ptr = nullptr;
	size = 0u;
}
template<class T>
inline T* Container<T>::data() noexcept{
	return ptr;
}
template<class T>
inline bool Container<T>::isEmpty()const noexcept{
	return !size;
}
template<class T>
inline size_t Container<T>::getSize() const noexcept{
	return size;
}
template<class T>
inline void Container<T>::clear() noexcept{
	delete[]ptr;
	__zero_clear();
}
template<class T>
inline void Container<T>::reserve(size_t sz) throw(std::bad_alloc){
	clear();
	if (sz == 0u)return;
	ptr = new T[sz];
	size = sz;
}
template<class T>
inline void Container<T>::set(T t) noexcept{
	if (isEmpty())return;
	for (size_t i = 0; i < size; i++)
		ptr[i] = t;
}

template<class T>
inline void Container<T>::set(const T* p, size_t sz) noexcept{
	if (sz > size)reserve(sz);
	for (size_t i = 0; i < size; i++) {
		ptr[i] = p[i];
	}
}

template<class T>
inline Container<T>::Container(){
	__zero_clear();
}

template<class T>
inline Container<T>::Container(size_t sz) throw(std::bad_alloc){
	__zero_clear();
	if (sz != 0u)reserve(sz);
}

template<class T>
inline Container<T>::~Container(){
	if (!isEmpty())
		clear();
}

template<class T>
inline Container<T>::Container(const Container& obj) noexcept{
	*this = obj;
}

template<class T>
inline Container<T>& Container<T>::operator=(const Container& obj) noexcept{
	reserve(obj.size);
	for (size_t i = 0; i < size; i++) {
		ptr[i] = obj.ptr[i];
	}
	return *this;
}

template<class T>
inline Container<T>::Container(Container&& obj) noexcept{
	*this = std::move(obj);
}

template<class T>
inline Container<T>& Container<T>::operator=(Container&& obj) noexcept{
	ptr  = obj.ptr;
	size = obj.size;
	obj.ptr  = nullptr;
	obj.size = 0u;
	return *this;
}

template<class T>
inline Container<T>::operator size_t() const noexcept{
	return size;
}

template<class T>
inline Container<T>::operator const T* () const noexcept{
	return ptr;
}

template<class T>
inline Container<T>::operator T* () noexcept{
	return ptr;
}

}