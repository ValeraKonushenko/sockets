#pragma once
#include <string>
#include <time.h>

namespace vk {

class Time {
private:
	Time()							= delete;
	~Time()							= delete;
	Time& operator=(const Time&)	= delete;
	Time& operator=(Time&&)			= delete;
	Time(const Time&)				= delete;
	Time(Time&&)					= delete;
public:
	static std::string getLocalTime(time_t unixtime);
	static std::string getLocalTime();
};

}
