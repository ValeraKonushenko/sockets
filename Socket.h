#pragma once
#include "_WSAData.h"
#include "Logger.h"
#include "SocketConnection.h"
#include "Buffer.h"
#include <mutex>



namespace vk::tcp::safe{
	class Socket {
	public:
		typedef int port;
		typedef vk::safe::Buffer Buffer;
	private:
	protected:
		mutable std::recursive_mutex	recmut;

		static WSADATA*			wsa;
		vk::safe::SocketConnection	sc;
		bool				createSocket	(port p)					noexcept;
		virtual void		printStep(const char*, bool smth = false)	const noexcept;
	public:
		void send(SOCKET out, const Buffer& bf, size_t size, int flags = 0)const  noexcept;
		bool reciev(SOCKET client, Buffer& buff, int size, int flags = 0)const noexcept;

		void				close			()							noexcept;
		bool				isOpen			()							const noexcept;
		vk::safe::SocketConnection	getConnection	()					const noexcept;
		Socket();
		operator SOCKET()												const noexcept;
		virtual ~Socket()												= default;
		Socket(const Socket&)											noexcept;
		Socket& operator=(const Socket&)								noexcept;
		Socket(Socket&&)												noexcept;
		Socket& operator=(Socket&&)										noexcept;
	};
}

namespace vk::tcp::unsafe {
	class Socket {
	public:
		typedef int port;
		typedef vk::unsafe::Buffer Buffer;
	private:
	protected:
		static WSADATA*			wsa;
		vk::unsafe::SocketConnection	sc;
			
		bool				createSocket	(port p)					noexcept;
		virtual void		printStep(const char*, bool smth = false)	const noexcept;
	public:
		void send(SOCKET out, const Buffer& bf, size_t size, int flags = 0)const  noexcept;
		void reciev(SOCKET client, Buffer& buff, int size, int flags = 0)const noexcept;
		Buffer reciev(SOCKET client, int size, int flags = 0)	const noexcept;

		void				close			()							noexcept;
		bool				isOpen			()							const noexcept;
		vk::unsafe::SocketConnection	getConnection	()				const noexcept;
		Socket();
		operator SOCKET()												const noexcept;
		virtual ~Socket()												= default;
		Socket(const Socket&)											noexcept;
		Socket& operator=(const Socket&)								noexcept;
		Socket(Socket&&)												noexcept;
		Socket& operator=(Socket&&)										noexcept;
	};
}
