#pragma once
#include <functional>
#include <atomic>


namespace vk::safe{
	class Subscriber {
	public:
		typedef std::atomic<unsigned long long> hash_t;
		typedef std::function<void()> func_t;
		typedef unsigned int event_id;
	private:
		const hash_t	hash;
		const func_t	func;
		const event_id	id;
		static hash_t	hash_counter;
	public:
						Subscriber			(event_id id,func_t event_func)	noexcept;
		hash_t			getHash				()								const noexcept;
		event_id		getEventId			()								const noexcept;
		void			update				()								const noexcept;
		bool			operator==			(const Subscriber& sb)			const noexcept;
		bool			operator!=			(const Subscriber& sb)			const noexcept;

		virtual			~Subscriber			()								= default;
						Subscriber			(const Subscriber& sb)			= delete;
						Subscriber			(Subscriber&& sb)				noexcept;
						Subscriber& operator=(const Subscriber& sb)			= delete;
						Subscriber& operator=(Subscriber&& sb)				noexcept;

	};
}



namespace vk::unsafe {
	class Subscriber {
	public:
		typedef unsigned long long hash_t;
		typedef std::function<void()> func_t;
		typedef unsigned int event_id;
	private:
		const hash_t	hash;
		const func_t	func;
		const event_id	id;
		static hash_t	hash_counter;
	public:
						Subscriber			(event_id id,func_t event_func)	noexcept;
		hash_t			getHash				()								const noexcept;
		event_id		getEventId			()								const noexcept;
		void			update				()								const noexcept;
		bool			operator==			(const Subscriber& sb)			const noexcept;
		bool			operator!=			(const Subscriber& sb)			const noexcept;

		virtual			~Subscriber			()								= default;
						Subscriber			(const Subscriber& sb)			= delete;
						Subscriber			(Subscriber&& sb)				noexcept;
						Subscriber& operator=(const Subscriber& sb)			= delete;
						Subscriber& operator=(Subscriber&& sb)				noexcept;

	};
}