#include "Buffer.h"

namespace vk::safe {
void Buffer::__init_clear()	noexcept {
	arr = nullptr;
	size = 0;
}
void					Buffer::clear		()noexcept {
	std::lock_guard<std::recursive_mutex> lg(recmut);
	if (arr == nullptr)return;
	delete[]arr;
	arr		= nullptr;
	size	= 0;
}
void					Buffer::alloc		(size_t len) {
	std::lock_guard<std::recursive_mutex> lg(recmut);
	if (len == 0)return;
	clear();
	arr = new char[size = len];
	if (!arr) {
		clear();
		throw std::exception("error->vk::Buffer: Bad alloc");
	}
}
void					Buffer::realloc		(size_t len) {
	std::lock_guard<std::recursive_mutex> lg(recmut);
	size_t tmp_size = len;
	bufftype* tmp = new bufftype[tmp_size];
	if (!arr) {
		throw std::exception("error->vk::Buffer: Bad alloc");
	}
	memcpy(tmp, this->arr, size);
	clear();
	this->size = tmp_size;
	this->arr  = tmp;
}
void					Buffer::set			(bufftype ch)noexcept {
	std::lock_guard<std::recursive_mutex> lg(recmut);
	memset(arr, ch, size * sizeof(bufftype));
}
void					Buffer::set			(const bufftype* buff, size_t psize)noexcept {
	std::lock_guard<std::recursive_mutex> lg(recmut);
	if (!buff)return;

	clear();
	alloc(psize);
	set(0);
	for (size_t i = 0; i < psize && i < size; i++) {
		arr[i] = buff[i];
	}
}
void					Buffer::set			(Buffer& buff)noexcept {

	std::lock_guard<std::recursive_mutex> lg(recmut);

	set(buff.arr, buff.size);
}
bool					Buffer::isEmpty		()const noexcept{

	std::lock_guard<std::recursive_mutex> lg(recmut);

	return arr == nullptr;
}
size_t					Buffer::lenght		()const{

	std::lock_guard<std::recursive_mutex> lg(recmut);

	if(sizeof(bufftype) == 1)
		return strlen(arr);
	else {
		throw std::exception("No possible to measure the string's length");
	}
}
size_t					Buffer::getSize		() const noexcept{

	std::lock_guard<std::recursive_mutex> lg(recmut);

	return this->size;
}
Buffer&					Buffer::operator=	(std::string str) noexcept{

	std::lock_guard<std::recursive_mutex> lg(recmut);

	*this = str.c_str();
	return *this;
}
Buffer&					Buffer::operator+=	(std::string str) noexcept{

	std::lock_guard<std::recursive_mutex> lg(recmut);

	*this += str.c_str();	
	return *this;
}
Buffer&					Buffer::operator+=	(const bufftype* str) noexcept{

	std::lock_guard<std::recursive_mutex> lg(recmut);

	size_t len = strlen(str);
	if (!len)return *this;
	realloc(size + len);
	memcpy(this->arr + strlen(this->arr), str, len);
	this->arr[size - 1] = 0;

	return *this;
}
Buffer&					Buffer::operator=	(const bufftype* str) noexcept{

	std::lock_guard<std::recursive_mutex> lg(recmut);

	clear();
	size_t len = strlen(str);
	if (!len)return *this;
	alloc(len + 1);
	memcpy(this->arr, str, len);
	this->arr[len] = 0;
	return *this;
}
Buffer::Cont			Buffer::get			()const noexcept {
	std::lock_guard<std::recursive_mutex> lg(recmut);

	if (isEmpty())return Cont();

	Cont tmp;
	tmp.set(arr, size);	
	return std::move(tmp);
}
Buffer::bufftype		Buffer::operator[]	(size_t pos)const noexcept {

	std::lock_guard<std::recursive_mutex> lg(recmut);

	while (pos >= size) pos -= size;
	return arr[pos];
}
Buffer::bufftype		Buffer::setAt		(size_t pos, bufftype ch)noexcept {

	std::lock_guard<std::recursive_mutex> lg(recmut);

	while (pos >= size) pos -= size;
	bufftype old = arr[pos];
	arr[pos] = ch;
	return old;
}
void					Buffer::swap		(Buffer& buff)noexcept {

	std::lock_guard<std::recursive_mutex> lg(recmut);

	Buffer tmp = buff;
	buff = *this;
	*this = tmp;
}
Buffer::Buffer								(size_t start_len)noexcept {
	__init_clear();
	if (start_len == 0)return;
	alloc(start_len);
	set(0);
}
Buffer::Buffer								(const bufftype* str) noexcept{
	std::lock_guard lg(recmut);
	__init_clear();
	*this = str;
}
Buffer::Buffer								(const Buffer& sc)noexcept {
	std::scoped_lock sl(recmut, sc.recmut);
	__init_clear();
	*this = sc;
}
Buffer::~Buffer								()noexcept {
	std::lock_guard lg(recmut);
	clear();
}
Buffer::Buffer								(Buffer&& sc)noexcept {
	std::scoped_lock sl(recmut, sc.recmut);
	this->__init_clear();
	*this = std::move(sc);
}
Buffer&					Buffer::operator=	(const Buffer& sc)noexcept {
	std::scoped_lock sl(recmut, sc.recmut);
	this->clear();

	arr = sc.arr;
	size = sc.size;
	return *this;
}
Buffer&					Buffer::operator=	(Buffer&& sc)noexcept {
	std::scoped_lock sl(recmut, sc.recmut);
	this->clear();
	arr = sc.arr;
	size = sc.size;
	sc.arr = nullptr;
	sc.size = 0;
	return *this;
}
						Buffer::operator size_t() const noexcept{
	std::lock_guard lg(recmut);
	return size;
}
}









namespace vk::unsafe {
void Buffer::__init_clear()	noexcept {
	arr = nullptr;
	size = 0;
}
void					Buffer::clear		()noexcept {
	if (arr == nullptr)return;
	delete[]arr;
	arr		= nullptr;
	size	= 0;
}
void					Buffer::alloc		(size_t len) {
	if (len == 0)return;
	clear();
	arr = new char[size = len];
	if (!arr) {
		clear();
		throw std::exception("error->vk::Buffer: Bad alloc");
	}
}
void					Buffer::realloc		(size_t len) {
	size_t tmp_size = len;
	bufftype* tmp = new bufftype[tmp_size];
	if (!arr) {
		throw std::exception("error->vk::Buffer: Bad alloc");
	}
	memcpy(tmp, this->arr, size);
	clear();
	this->size = tmp_size;
	this->arr  = tmp;
}
void					Buffer::set			(bufftype ch)noexcept {
	memset(arr, ch, size * sizeof(bufftype));
}
void					Buffer::set			(const bufftype* buff, size_t psize)noexcept {
	if (!buff)return;

	clear();
	alloc(psize);
	set(0);
	for (size_t i = 0; i < psize && i < size; i++) {
		arr[i] = buff[i];
	}
}
void					Buffer::set			(Buffer& buff)noexcept {
	set(buff.arr, buff.size);
}
bool					Buffer::isEmpty		()const noexcept{
	return arr == nullptr;
}
size_t					Buffer::lenght		()const{
	if(sizeof(bufftype) == 1)
		return strlen(arr);
	else {
		throw std::exception("No possible to measure the string's length");
	}
}
size_t					Buffer::getSize		() const noexcept{
	return this->size;
}
Buffer&					Buffer::operator=	(std::string str) noexcept{
	*this = str.c_str();
	return *this;
}
Buffer&					Buffer::operator+=	(std::string str) noexcept{
	*this += str.c_str();	
	return *this;
}
Buffer&					Buffer::operator+=	(const bufftype* str) noexcept{
	size_t len = strlen(str);
	if (!len)return *this;
	realloc(size + len);
	memcpy(this->arr + strlen(this->arr), str, len);
	this->arr[size - 1] = 0;

	return *this;
}
Buffer&					Buffer::operator=	(const bufftype* str) noexcept{
	clear();
	size_t len = strlen(str);
	if (!len)return *this;
	alloc(len + 1);
	memcpy(this->arr, str, len);
	this->arr[len] = 0;
	return *this;
}
std::string				Buffer::get			()const noexcept {
	if (isEmpty())return std::string();
	std::string tmp(arr);
	return std::move(tmp);
}
Buffer::bufftype		Buffer::operator[]	(size_t pos)const noexcept {
	while (pos >= size) pos -= size;
	return arr[pos];
}
Buffer::bufftype		Buffer::setAt		(size_t pos, bufftype ch)noexcept {
	while (pos >= size) pos -= size;
	bufftype old = arr[pos];
	arr[pos] = ch;
	return old;
}
void					Buffer::swap		(Buffer& buff)noexcept {
	Buffer tmp = buff;
	buff = *this;
	*this = tmp;
}
Buffer::Buffer								(size_t start_len)noexcept {
	__init_clear();
	if (start_len == 0)return;
	alloc(start_len);
	set(0);
}
Buffer::Buffer								(const bufftype* str) noexcept{
	__init_clear();
	*this = str;
}
Buffer::Buffer								(const Buffer& sc)noexcept {
	__init_clear();
	*this = sc;
}
Buffer::~Buffer								()noexcept {
	clear();
}
Buffer::Buffer								(Buffer&& sc)noexcept {
	*this = std::move(sc);
}
Buffer&					Buffer::operator=	(const Buffer& sc)noexcept {
	arr = sc.arr;
	size = sc.size;
	return *this;
}
Buffer&					Buffer::operator=	(Buffer&& sc)noexcept {
	arr = sc.arr;
	size = sc.size;
	__init_clear();
	return *this;
}
						Buffer::operator size_t() const noexcept {
	return size;
}
}