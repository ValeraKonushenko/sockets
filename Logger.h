#pragma once
#include "Log.h"
#include "Observer.h"
#include <vector>
namespace vk::safe {
class Logger {
public:
	enum Events:int {
		push_log,
		pop_log
	};
private:
	typedef std::lock_guard<std::recursive_mutex> logmutex;
	static Observer observer;
	static std::recursive_mutex recmutex;
	static std::vector<Log>		logs;
protected:
public:
	static void				subscribe(Subscriber& sbr)			noexcept;
	static void				unsubscribe(const Subscriber& sbr)	noexcept;
	static void				push			(Log lg);
	static void				pop				();
	static const Log&		getLastLog		();
	static const Log&		getLog			(int i);
	
	Logger()							= delete;
	~Logger()							= delete;
	Logger(const Logger&)				= delete;
	Logger& operator=(const Logger&)	= delete;
	Logger(Logger&&)					= delete;
	Logger& operator=(Logger&&)			= delete;
};
}