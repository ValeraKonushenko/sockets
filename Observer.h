#pragma once
#include "Subscriber.h"
#include <mutex>

namespace vk::safe {
	class Observer {
	private:
	protected:
		mutable std::recursive_mutex mut;
		std::vector<Subscriber> vec;
	public:
		void			subscribe			(Subscriber& sbr)				noexcept;
		void			unsubscribe			(const Subscriber& sbr)			noexcept;
		void			notify				(Subscriber::event_id id)		const noexcept; 
		virtual			~Observer			()								= default;
						Observer			()								= default;
						Observer			(const Observer& sb)			noexcept;
						Observer			(Observer&& sb)					noexcept;
						Observer& operator=	(const Observer& sb)			noexcept;
						Observer& operator=	(Observer&& sb)					noexcept;
	};
}

namespace vk::unsafe {
	class Observer {
	private:
	protected:
		std::vector<Subscriber> vec;
	public:
		void			subscribe			(Subscriber& sbr)				noexcept;
		void			unsubscribe			(const Subscriber& sbr)			noexcept;
		void			notify				(Subscriber::event_id id)		const noexcept; 
		virtual			~Observer			()								= default;
						Observer			()								= default;
		//				Observer			(const Observer& sb)			noexcept;
						Observer			(Observer&& sb)					noexcept;
		//				Observer& operator=	(const Observer& sb)			noexcept;
						Observer& operator=	(Observer&& sb)					noexcept;
	};
}
