#include "Log.h"
#include "Time.h"
namespace vk {
Log::Log(std::string msg, errorID eid) {
	clear();
	if (msg.length() > 0)
		setLog(msg, eid);
}
void			Log::clear		(){
	std::lock_guard _lg(recmut);
	_error.id_error = static_cast<errorID>(0);
	msg.clear();
	log_time = {};
}
void			Log::setLog		(std::string msg, errorID eid){
	std::lock_guard _lg(recmut);
	this->msg = msg;
	this->_error.id_error = eid;
	this->log_time = ::time(nullptr);
}
std::string		Log::message	() const{
	std::lock_guard _lg(recmut);
	return msg;
}
Log::Error		Log::error		() const{
	std::lock_guard _lg(recmut);
	return Error(this->_error);
}
bool			Log::isError	() const{
	std::lock_guard _lg(recmut);
	return static_cast<bool>(_error.is_error);
}
time_t			Log::getTimeUTC	() const{
	std::lock_guard _lg(recmut);
	return log_time;
}
std::string		Log::getTime	() const{
	std::lock_guard _lg(recmut);
	return std::string(vk::Time::getLocalTime(this->log_time));
}
Log::Log						(const Log& lg)	{
	*this = lg;
}
Log::Log						(Log&& lg)noexcept {
	*this = std::move(lg);
}
Log&			Log::operator=	(const Log& lg)noexcept{
	std::scoped_lock sl(recmut, lg.recmut);
	this->log_time = lg.log_time;
	this->msg = lg.msg;
	this->_error = lg._error;
	return *this;
}
Log&			Log::operator=	(Log&& lg)noexcept {
	std::scoped_lock sl(recmut, lg.recmut);
	this->log_time	= lg.log_time;
	this->msg		= std::move(lg.msg);
	this->_error	= lg._error;
	lg.log_time = static_cast<time_t>(0);
	memset(&lg._error, 0, sizeof(lg._error));
	return *this;
}
}