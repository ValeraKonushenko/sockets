#include "Subscriber.h"

namespace vk::safe {
	Subscriber::hash_t Subscriber::hash_counter = 0;

	Subscriber::Subscriber(Subscriber::event_id id, Subscriber::func_t event_func)
		noexcept :
		hash(hash_counter.load()),
		func(event_func),
		id(id)  {
		hash_counter++;
	}
	Subscriber::hash_t Subscriber::getHash()const noexcept {
		return hash.load();
	}
	Subscriber::event_id Subscriber::getEventId()const noexcept {
		return id;
	}
	void Subscriber::update()const noexcept {
		if(func)func();
	}
	bool Subscriber::operator==(const Subscriber& sb)const noexcept {
		return id == sb.id && hash == sb.hash;
	}
	bool Subscriber::operator!=(const Subscriber& sb)const noexcept {
		return id != sb.id && hash != sb.hash;
	}
	Subscriber::Subscriber(Subscriber&& sb) noexcept :
		hash(sb.hash.load()),
		func(sb.func),
		id	(sb.id){
		memset(&sb, 0, sizeof(sb));
	}
	Subscriber& Subscriber::operator=(Subscriber&& sb) noexcept	{		
		size_t len = sizeof(*this);
		memcpy_s(this, len, &sb, len);
		return *this;
	}
}


namespace vk::unsafe {
	Subscriber::hash_t Subscriber::hash_counter = 0;

	Subscriber::Subscriber(Subscriber::event_id id, Subscriber::func_t event_func) noexcept :
		hash(hash_counter),
		func(event_func),
		id(id)  {
		hash_counter++;
	}
	Subscriber::hash_t Subscriber::getHash()const noexcept {
		return hash;
	}
	Subscriber::event_id Subscriber::getEventId()const noexcept {
		return id;
	}
	void Subscriber::update()const noexcept {
		func();
	}
	bool Subscriber::operator==(const Subscriber& sb)const noexcept {
		return id == sb.id && hash == sb.hash;
	}
	bool Subscriber::operator!=(const Subscriber& sb)const noexcept {
		return id != sb.id && hash != sb.hash;
	}
	Subscriber::Subscriber(Subscriber&& sb) noexcept :
		hash(sb.hash),
		func(sb.func),
		id(sb.id) {
		memset(&sb, 0, sizeof(sb));
	}
	Subscriber& Subscriber::operator=(Subscriber&& sb) noexcept {
		size_t len = sizeof(*this);
		memcpy_s(this, len, &sb, len);
		return *this;
	}
}