#include "Socket.h"
#include <string>
#pragma warning(disable: 4100)
namespace vk::tcp::safe {
WSADATA* Socket::wsa = nullptr;
bool Socket::createSocket(port p) noexcept{
	std::lock_guard lg(recmut);
	try {
		sc.socket(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP));	
	} catch (const std::exception& ex) {
		vk::safe::Logger::push({ex.what()});
	}
	if (sc.socket() == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Creating of the socket was failed")));
		return false;
	}
	sc.port(p);
	printStep("A socket was created");
	return true;
}
void	Socket::printStep(const char*msg, bool smth)const noexcept{
	std::lock_guard lg(recmut);
	vk::safe::Logger::push(Log(msg));
}
void	Socket::send(SOCKET out, const Socket::Buffer& bf, size_t size, int flags) const noexcept{
	::send(
		out,
		reinterpret_cast<const char*>(bf.get().data()), 
		static_cast<int>(size),
		flags
	);
}

bool	Socket::reciev
		(SOCKET client, Socket::Buffer& buff, int size, int flags) const noexcept {
	bool is_get = false;
	char* tbuff = new char[size] {};
	int len = ::recv(client, tbuff, size, flags);
	if (len != -1)
		is_get = true;

	buff.set(tbuff, size);
	delete[]tbuff;
	return is_get;
}
//Socket::Buffer	Socket::reciev			(SOCKET client, int size, int flags) const noexcept{
//	std::lock_guard lg(recmut);
//	Socket::Buffer buff;
//	char* tbuff = new char[size] {};
//	::recv(client, tbuff, size, flags);
//	buff.set(tbuff, size);
//	delete[] tbuff;
//	return std::move(buff);
//}
void	Socket::close			() noexcept {
	std::lock_guard lg(recmut);
	closesocket(sc.socket());
	sc.clear();
}
bool	Socket::isOpen			()const  noexcept {
	std::lock_guard lg(recmut);
	return sc.isInvalidSocket();
}
vk::safe::SocketConnection Socket::getConnection()const noexcept {
	std::lock_guard lg(recmut);
	return sc;
}
		Socket::Socket			() {
	try {
		wsa	= _WSAData::startup();
	} catch (const std::exception&ex) {
		vk::safe::Logger::push(std::move(Log(ex.what())));
	}
	close();
}
		Socket::operator SOCKET() const noexcept{
			std::lock_guard lg(recmut);
			return this->sc;
		}
		Socket::Socket			(const Socket& s) noexcept {
	std::lock_guard lg(recmut);
	*this = s;
}
		Socket& Socket::operator=(const Socket& s) noexcept {
	std::lock_guard lg(recmut);
	this->wsa = s.wsa;
	this->sc = s.sc;
	return *this;
}
		Socket::Socket			(Socket&& s) noexcept {
	std::lock_guard lg(recmut);
	*this = std::move(s);
}
		Socket& Socket::operator=(Socket&& s)  noexcept {
	std::lock_guard lg(recmut);
	this->wsa = s.wsa;
	this->sc  = s.sc;
	return *this;
}

}


namespace vk::tcp::unsafe {
WSADATA* Socket::wsa = nullptr;
void	Socket::printStep		(const char*msg, bool smth)const noexcept{
	vk::safe::Logger::push(Log(msg));
}
void	Socket::send			(SOCKET out, const Socket::Buffer& bf, size_t size, int flags) const noexcept{
	::send(
		out,
		reinterpret_cast<const char*>(bf.get().data()),
		static_cast<int>(size),
		flags
	);
}
bool	Socket::createSocket	(port p) noexcept {
	sc.socket(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP));
	if (sc.socket() == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Creating of the socket was failed")));
		return false;
	}
	sc.port(p);
	printStep("A socket was created");
	return true;
}
void	Socket::reciev		
		(SOCKET client, Socket::Buffer& buff, int size, int flags) const noexcept {
	char* tbuff = new char[size] {};
	::recv(sc.socket(), tbuff, size, flags);
	buff.set(tbuff, size);
	delete[]tbuff;
}
Socket::Buffer	Socket::reciev			(SOCKET client, int size, int flags) const noexcept{
	Buffer buff;
	char* tbuff = new char[size];
	::recv(client, tbuff, size, flags);
	buff.set(tbuff, size);
	delete[] tbuff;
	return std::move(buff);
}
void	Socket::close			() noexcept {
	closesocket(sc.socket());
	sc.clear();
}
bool	Socket::isOpen			()const  noexcept {
	return sc.isInvalidSocket();
}
vk::unsafe::SocketConnection Socket::getConnection()const noexcept {
	return sc;
}
		Socket::Socket			() {
	try {
		wsa	= _WSAData::startup();
	} catch (const std::exception&ex) {
		vk::safe::Logger::push(std::move(Log(ex.what())));
	}
	close();
}
		Socket::Socket			(const Socket& s) noexcept {
	*this = s;
}
		Socket& Socket::operator=(const Socket& s) noexcept {
	this->wsa = s.wsa;
	this->sc = s.sc;
	return *this;
}
		Socket::Socket			(Socket&& s) noexcept {
	*this = std::move(s);
}
		Socket& Socket::operator=(Socket&& s)  noexcept {
	this->wsa = s.wsa;
	this->sc  = s.sc;
	return *this;
}
		Socket::operator SOCKET	()const noexcept {
			return sc;
		}

}
#pragma warning(default: 4100)