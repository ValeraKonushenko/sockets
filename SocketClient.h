#pragma once
#include "Socket.h"

namespace vk::tcp::safe {
	class SocketClient : public vk::tcp::safe::Socket{
	private:
	protected:
		bool	createSin(PCWSTR addr);
	public:
		bool	open(port p, PCWSTR addr)				noexcept;
				SocketClient()							= default;
		void	send(const Buffer& bf, size_t size, int flags = 0)	const  noexcept;
		bool	connect()								noexcept;
		std::string getInetNtop()						const noexcept;
		virtual ~SocketClient()							= default;
		SocketClient(const SocketClient&)			;
		SocketClient& operator=(const SocketClient&);
		SocketClient(SocketClient&&)				;
		SocketClient& operator=(SocketClient&&)		;

	};
}


namespace vk::tcp::unsafe {
class SocketServer : public vk::tcp::unsafe::Socket {
private:
protected:
	bool _bind()									noexcept;
	bool _listen()									noexcept;
public:
	bool	open(port p, PCWSTR addr)				noexcept;
	SocketServer()									= default;
	virtual ~SocketServer()							= default;
	SocketServer(const SocketServer&);
	SocketServer& operator=(const SocketServer&);
	SocketServer(SocketServer&&);
	SocketServer& operator=(SocketServer&&);

};
}
