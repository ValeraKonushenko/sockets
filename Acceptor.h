#pragma once
#include "Logger.h"
#include "SocketConnection.h"
#include "_WSAData.h"
#include <vector>


namespace vk::safe {
class Acceptor {
private:
protected:
	mutable std::recursive_mutex	recmut;

	std::vector<SocketConnection> vec;
public:
	void				accept		(SocketConnection sc);
	size_t				size		()						const;
	SocketConnection	operator[]	(int)					const noexcept;
	SocketConnection	end			()						const noexcept;
	void				dropConnection(int pos)					noexcept;

	Acceptor()												=default;
	virtual ~Acceptor				()						=default;
	Acceptor						(const Acceptor&obj)	noexcept;
	Acceptor& operator=				(const Acceptor&obj)	noexcept;
	Acceptor						(Acceptor&&obj)			noexcept;
	Acceptor& operator=				(Acceptor&&obj)			noexcept;
};
}



namespace vk::unsafe {
class Acceptor {
private:
protected:
	std::vector<SocketConnection> vec;
public:
	void				accept		(SocketConnection sc);
	size_t				size		()						const;
	SocketConnection	operator[]	(int)					const noexcept;
	SocketConnection	end			()						const noexcept;

	Acceptor()												= default;
	virtual ~Acceptor				()						= default;
	Acceptor						(const Acceptor&obj)	noexcept;
	Acceptor& operator=				(const Acceptor&obj)	noexcept;
	Acceptor						(Acceptor&&obj)			noexcept;
	Acceptor& operator=				(Acceptor&&obj)			noexcept;
};
}