#include "Logger.h"
namespace vk::safe {

std::vector<Log>		Logger::logs{};
std::recursive_mutex	Logger::recmutex{};
Observer				Logger::observer;

void Logger::subscribe(Subscriber& sbr) noexcept{
	logmutex _lg(recmutex);
	observer.subscribe(sbr);
}

void Logger::unsubscribe(const Subscriber& sbr) noexcept{
	logmutex _lg(recmutex);
	observer.unsubscribe(sbr);
}

void			Logger::push			(Log lg){
	logmutex _lg(recmutex);
	logs.push_back(std::move(lg));
	observer.notify(Logger::Events::push_log);
}
void			Logger::pop				(){
	logmutex lg(recmutex);
	logs.pop_back();
	observer.notify(Logger::Events::pop_log);
}
const Log&		Logger::getLastLog		(){
	logmutex lg(recmutex);
	return logs.back();
}
const Log&		Logger::getLog			(int i){
	logmutex lg(recmutex);
	int size = static_cast<int>(logs.size());
	while (i >= size)	i -= size;
	while (i < 0)		i += size;
	return logs[i];
}


}
