#include "SocketServer.h"




namespace vk::tcp::safe {
bool SocketServer::_bind()noexcept {
	std::lock_guard lg(recmut);

	SOCKADDR_IN sin;
	sin.sin_family			= AF_INET;
	sin.sin_addr.s_addr		= htonl(INADDR_ANY);
	sin.sin_port			= htons(static_cast<u_short>(sc.port()));


	if (bind(sc.socket(), (LPSOCKADDR)&sin, sizeof(sin)) == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Binding of the socket was failed" )));
		return false;
	}
	sc.sockaddr(sin);
	printStep("Binding was successful");
	return true;
}
bool SocketServer::_listen() noexcept{
	std::lock_guard lg(recmut);

	if (listen(sc.socket(), SOMAXCONN) == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Listening of the socket was failed")));
		return false;
	}

	printStep("Listening was successful");

	return true;
}
bool SocketServer::open(port p)noexcept {
	std::lock_guard lg(recmut);
	if (!this->createSocket(p))	return false;
	if (!this->_bind())			return false;
	if (!this->_listen())		return false;
	printStep((
		std::string("Server's port: ") + 
		std::to_string(p) + 
		std::string(" was successful opened")).c_str());
	return true;
}
SocketServer::SocketServer(const SocketServer& s) {
	std::lock_guard lg(this->recmut);
	*this = s;
}
SocketServer& SocketServer::operator=(const SocketServer& s) {
	std::lock_guard lg(recmut);
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}
SocketServer::SocketServer(SocketServer&& s) {
	std::lock_guard lg(recmut);
	*this = std::move(s);
}
SocketServer& SocketServer::operator=(SocketServer&& s) {
	std::lock_guard lg(recmut);
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}

}




namespace vk::tcp::unsafe {
bool SocketServer::_bind()noexcept {
	SOCKADDR_IN sin;
	sin.sin_family			= AF_INET;
	sin.sin_addr.s_addr		= htonl(INADDR_ANY);
	sin.sin_port			= htons(static_cast<u_short>(sc.port()));


	if (bind(sc.socket(), (LPSOCKADDR)&sin, sizeof(sin)) == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Binding of the socket was failed" )));
		return false;
	}
	sc.sockaddr(sin);
	printStep("Binding was successful");
	return true;
}

bool SocketServer::_listen() noexcept{
	if (listen(sc.socket(), SOMAXCONN) == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Listening of the socket was failed")));
		return false;
	}

	printStep("Listening was successful");

	return true;
}
bool SocketServer::open(port p)noexcept {
	if (!this->createSocket(p))	return false;
	if (!this->_bind())			return false;
	if (!this->_listen())		return false;
	printStep((
		std::string("Port: ") + std::to_string(p) + std::string(" was successful opened")
		).c_str());
	return true;
}
SocketServer::SocketServer(const SocketServer& s) {
	*this = s;
}
SocketServer& SocketServer::operator=(const SocketServer& s) {
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}
SocketServer::SocketServer(SocketServer&& s) {
	*this = std::move(s);
}
SocketServer& SocketServer::operator=(SocketServer&& s) {
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}

}